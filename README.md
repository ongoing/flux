FLUX 8.2.2 patched version
=========

Enables PHP 7.2 Support on Typo3 < 8


GET patched flux Extension
---------------
`git clone git@bitbucket.org:ongoing/flux.git typo3conf/ext/_patched_flux`


Replace original EXT with patched EXT
---------------
`mv typo3conf/ext/flux typo3conf/ext/_original_flux && mv typo3conf/ext/_patched_flux typo3conf/ext/flux`

